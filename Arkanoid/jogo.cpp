#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <stdlib.h>
#define LARGURA 1000
#define ALTURA 700
#include <math.h>

//controle de colisao atraves de bounding box
//Enter
//contagem de pontos soma dos pontos
//vidas = so pode morrer 3 vezes

SDL_Window *janelaprincipal = NULL; // nome da janela principal do jogo
SDL_Surface *content = NULL; // �rea de trabalho da janela

SDL_Renderer *visualizacao;
SDL_Renderer *visualizacao1;

SDL_Window *janelafim = NULL;

SDL_Surface *textSurface;
SDL_Texture *text;
SDL_Rect textRect;

//controle do teclado
typedef struct Teclas{
    bool esquerda;
    bool direita;
    bool espaco;
    bool enter;
};

Teclas teclas;

void inicializaTeclas(){
    teclas.esquerda=false;
    teclas.direita=false;
    teclas.espaco=false;
    teclas.enter=false;
}

typedef struct Bola{
    SDL_Rect area; // cria um ret�ngulo area.x, area.y, area.w (largurra), area.h (altura)
    float velocidade; // velocidade que o objeto se move
    SDL_Texture* textura=NULL; // textura da imagem
    float altura;
    float largura;
    bool emmovimento; // para/reinicia movimento
    float angulo; //angulo que a bola ir� se movimentar
    float dirx, diry;
};

typedef struct Base{
    SDL_Rect area; // cria um ret�ngulo area.x, area.y, area.w (largurra), area.h (altura)
    float velocidade; // velocidade que o objeto se move
    SDL_Texture* textura=NULL; // textura da imagem
    float altura;
    float largura;
};

typedef struct Bloco{
    SDL_Rect area; // cria um ret�ngulo area.x, area.y, area.w (largurra), area.h (altura)
    float velocidade; // velocidade que o objeto se move
    SDL_Texture* textura=NULL; // textura da imagem
    int px; // linha da matriz
    int py; // coluna da matriz
    int vida; // qtd de hits para destruir
    int pontos; //quantos pontos vale quando destruir
};

typedef struct ObjetoDeCena{
    SDL_Rect area; // cria um ret�ngulo area.x, area.y, area.w (largurra), area.h (altura)
    SDL_Texture* textura=NULL; // textura da imagem
};

ObjetoDeCena *fundo;
Base base;
Bola bola;
Bloco b[50];//[coloca quantos blocos]
//diretivas do jogo
bool fim=false; // encerra o jogo com true
bool bolaemjogo=false;


// nome do objeto, posicao x, posi��o y, largula e altura
ObjetoDeCena* carregaObjeto(const char *caminho_da_imagem, float x, float y, int l, int a){
    ObjetoDeCena *o;
    o = (ObjetoDeCena*) malloc (sizeof (ObjetoDeCena));
    o->area.x = x;
    o->area.y = y;
    o->area.w = l; //largura do objeto
    o->area.h = a; //altura da nave
    SDL_Surface* imagem = IMG_Load(caminho_da_imagem); // le a imagem
    if( imagem == NULL ){
        printf( "Erro ao carregar imagem %s\n", SDL_GetError() );
    }else {
        o->textura = SDL_CreateTextureFromSurface(visualizacao, imagem); // cria a textura
        SDL_FreeSurface(imagem); // apaga a imagem da tela
    }
    return o;
}

void carregaBase(Base *o, const char *caminho_da_imagem, int velocidade){
    o->area.w=100; //largura da base
    o->area.h=25; //altura da base
    SDL_Surface* imagem = IMG_Load(caminho_da_imagem); // le a imagem
    if( imagem == NULL ){
        printf( "Erro ao carregar base %s\n", SDL_GetError() );
    }else {
        o->textura = SDL_CreateTextureFromSurface(visualizacao, imagem); // cria a textura
        SDL_FreeSurface(imagem); // apaga a imagem da tela
    }
    o->area.x=(LARGURA-o->area.w)/2;
    o->area.y=ALTURA-25;
    o->velocidade=velocidade;
}

void carregaBola(Bola *o, const char *caminho_da_imagem, int velocidade){
    o->area.w=16;
    o->area.h=16;
    SDL_Surface* imagem = IMG_Load(caminho_da_imagem);
    if( imagem == NULL ){
        printf( "Erro ao carregar bola %s\n", SDL_GetError() );
    }else {
        o->textura = SDL_CreateTextureFromSurface(visualizacao, imagem); // cria a textura
        SDL_FreeSurface(imagem); // apaga a imagem da tela
    }
    o->area.x=base.area.x+(base.area.w/2);
    o->area.y=base.area.y-o->area.h;
    o->velocidade=velocidade;
}

void carregaBloco(Bloco o, const char *caminho_da_imagem, int px, int py, int vida, int pontos){
    o.area.w=64; //largura do bloco
    o.area.h=32; //altura do bloco
    SDL_Surface* imagem = IMG_Load(caminho_da_imagem); // le a imagem
    if( imagem == NULL ){
        printf( "Erro ao carregar nave %s\n", SDL_GetError() );
    }else {
        o.textura = SDL_CreateTextureFromSurface(visualizacao, imagem); // cria a textura
        SDL_FreeSurface(imagem); // apaga a imagem da tela
    }
    o.px=px;
    o.py=py;
    o.vida=vida;
    o.pontos=pontos;
}

bool init()
{
    bool success = true;

    //Inicializa a SDL
    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        printf( "Erro ao carregar a SDL: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Cria a janela principal
        janelaprincipal = SDL_CreateWindow( "Arkanoid Demo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                   LARGURA, ALTURA, SDL_WINDOW_SHOWN );
        if( janelaprincipal == NULL )
        {
            printf( "Erro na cria��o da janela: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            visualizacao = SDL_CreateRenderer(janelaprincipal, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        }
        fundo = carregaObjeto("imagens\\background_0.jpg",0,0,LARGURA,ALTURA);
        carregaBase(&base, "imagens\\base.png",6);
        for(int i=0; i < 50; i++){
            int x = 1;
            int y = 1;
            carregaBloco(b[i], "imagens\\b0.png", x+i, y, 1, 1);
        }
        //carregaBloco(&b0,"imagens\\b0.png",1,1,1,5);
        //carregaBloco(&b1,"imagens\\b0.png",1,2,1,5);//criar varios blocos
        carregaBola(&bola,"imagens\\bola.png",4);
    }
    return success;
}

void desenhaBase(){
    SDL_RenderCopy(visualizacao,base.textura,NULL, &base.area);
}

void desenhaBackgound(){
    SDL_RenderCopy(visualizacao,fundo->textura,NULL, &fundo->area);
}

void desenhaBlocos(){
    for(int i=0; i < 50; i++){
        b[i].area.x=64*(b[i].px-1);
        b[i].area.y=64+32*(b[i].py-1);
        SDL_RenderCopy(visualizacao,b[i].textura,NULL, &b[i].area);;
    }

    //b0.area.x=64*(b0.px-1);
    //b0.area.y=64+32*(b0.py-1);

   //if vida = 0 mudar achar o codigo
//    SDL_RenderCopy(visualizacao,b0.textura,NULL, &b0.area);;
//    SDL_RenderCopy(visualizacao,b1.textura,NULL, &b1.area);;
}

void desenhaBola(){
    SDL_RenderCopy(visualizacao,bola.textura,NULL, &bola.area);
}

void close()
{
    SDL_FreeSurface( content );
    content = NULL;
    SDL_DestroyWindow( janelaprincipal );
    janelaprincipal= NULL;
    SDL_DestroyWindow( janelafim );
    janelafim = NULL;
    IMG_Quit();
    SDL_DestroyRenderer(visualizacao);
    SDL_DestroyRenderer(visualizacao1);
//    SDL_DestroyTexture(fundo.textura);
    SDL_Quit();
}

void colisaoBase(){
  if (base.area.x <= 0){
    base.area.x = 0;
  }
  /*if (base.area.x+base.area.w >= LARGURA-100){
    base.area.x = LARGURA-base.area.w;
  }*/
}


void colisaoBola(){
    if(bola.area.y <=0 ){
        bola.diry=(bola.diry*-1);
    }
    if(bola.area.y >= ALTURA ){
        bolaemjogo = false;
        //bola.diry=(bola.diry*-1);
    }
    if(bola.area.x <= 0 ){
        bola.dirx=(bola.dirx*-1);
    }
    if(bola.area.x >= LARGURA ){
        bola.dirx=(bola.dirx*-1);
    }
    if((bola.area.y >= base.area.y) && (bola.area.x >= base.area.x && bola.area.x <= base.area.x+base.area.w)){
        bola.diry=(bola.diry*-1);
    }
}

void moveBola(){
    if(bolaemjogo){
        bola.area.x += bola.dirx*bola.velocidade;
        bola.area.y += bola.diry*bola.velocidade;
        //bola.area.x-=1;
        //bola.area.y-=bola.velocidade;
        //colisaoBolaDireita();
        colisaoBase();
        colisaoBola();
    }
}


void display(){
    SDL_RenderClear(visualizacao);
    desenhaBackgound();
    moveBola();
    desenhaBase();
    desenhaBlocos();
    desenhaBola();
    SDL_RenderPresent(visualizacao);
}

void executaAcao()
{
    if(teclas.direita)
        //base.area.x+=base.velocidade;
        if (base.area.x+base.area.w <= LARGURA-base.velocidade){
            base.area.x += base.velocidade;
        }

    if(teclas.esquerda)
    {
        if(base.area.x>=base.velocidade)//fazer o mesmo direita mas considerar a largura x+w
            base.area.x-=base.velocidade;
        else
            base.area.x=0;
    }
    if(teclas.espaco){
        if(!bolaemjogo){
        bolaemjogo=true;
        bola.dirx = rand()%17*0.13;
        bola.diry = -rand()%12*0.13;
        teclas.espaco=false; //fazer um do enter
        }
    }
}

int main( int argc, char* args[] )
{
    inicializaTeclas();
    //Eventos
    SDL_Event evento;//monitora teclado mouse

    //Inicializa a SDL
    if( !init() )
    {
        printf( "Falha na inicializa��o!\n" );
    }
    else
    {
        while(!fim){
            while( SDL_PollEvent( &evento ))
            {
                switch(evento.type){
                    case SDL_QUIT :
                        fim = true;
                        break;

                    case SDL_KEYDOWN: // tecla pressionada
                        if(evento.key.keysym.sym == SDLK_LEFT)
                            teclas.esquerda=true;
                        if (evento.key.keysym.sym == SDLK_RIGHT)
                            teclas.direita=true;
                        if (evento.key.keysym.sym == SDLK_1)
                            teclas.enter=true;
                        if (evento.key.keysym.sym == SDLK_SPACE)
                            teclas.espaco=true;
                        if (evento.key.keysym.sym == SDLK_ESCAPE)
                            fim = true;
                        break;
                    case SDL_KEYUP: // tecla solta
                        if(evento.key.keysym.sym == SDLK_LEFT)
                            teclas.esquerda=false;
                        if (evento.key.keysym.sym == SDLK_RIGHT)
                            teclas.direita=false;
                        if (evento.key.keysym.sym == SDLK_1)
                            teclas.enter=false;
                        if (evento.key.keysym.sym == SDLK_SPACE)
                            teclas.espaco=false;
                        break;
                }
            }
            executaAcao();
            display();
        }
    }

    close();

    return 0;
}

